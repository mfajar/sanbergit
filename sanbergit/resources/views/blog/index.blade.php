@extends('layouts.master')

@section('main')
	<h1>Username : {{$data[0]->namespace->name}}</h1>
@foreach($data as $projek)
<div class="card mb-3 mt-4" style="max-width: 18rem;">
  <div class="card-header bg-transparent border-success"><h3>{{$projek ->name}}</h3></div>
  <div class="card-body">
    <h5 class="card-title">{{$projek->name_with_namespace}}</h5>

    <p class="card-text">Deskripsi : {!!$projek->description!!}</p>
    <p>	<a href="/project/star/{{$projek->id}}"><i class="fa fa-star"></i></a>= {{$projek->star_count}}</p>
  </div>
  <div class="card-footer bg-transparent border-success"><a class="btn btn-primary" href="{{$projek->web_url}}">View Project</a></div>
</div>
@endforeach
@endsection