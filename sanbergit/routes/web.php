<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index');
Route::get('/project/{id}', 'BlogController@project');
Route::get('/project/star/{id}', 'BlogController@star');
Route::get('/project/new/{id}', 'BlogController@new');
