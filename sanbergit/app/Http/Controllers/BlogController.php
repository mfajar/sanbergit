<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class BlogController extends Controller
{
    public function index()
    {
    	return redirect('/project/mfajar');
    }
    public function project($id)
    {
        $client = new Client(); //GuzzleHttp\Client
        $url = 'https://gitlab.com/api/v4/users/'.$id.'/projects';
        $header = array('headers'=>array('privat_token' => 'ZgXvWqNksvVBVgxApvak'));
        $response = $client->get($url,$header);
         $data =json_decode($response->getBody()->getContents());
         return view('blog.index',compact('data'));
    }
    public function star($id)
    {    $client = new Client();
        //project star dengan id
        $url = 'https://gitlab.com/api/v4/projects/'.$id.'/star';
        $header = array('headers'=>array('PRIVATE-TOKEN' => 'ZgXvWqNksvVBVgxApvak'));
        $response = $client->post($url,$header); 
        //ambil data project cari username
        $url = 'https://gitlab.com/api/v4/projects/'.$id;
        $header = array('headers'=>array('PRIVATE-TOKEN' => 'ZgXvWqNksvVBVgxApvak'));
        $response = $client->get($url,$header);
        $data =json_decode($response->getBody()->getContents());    
        $username = $data->owner->username;
         return redirect('/project/'.$username);
    }
    public function new($name)
    {
        $client = new Client(); //GuzzleHttp\Client
        $url = 'https://gitlab.com/api/v4/projects?name='.$name.'&visibility=public';
        $header = array('headers'=>array('PRIVATE-TOKEN' => 'ZgXvWqNksvVBVgxApvak'));
        $response = $client->request('POST',$url,$header);
        //$data =json_decode($response->getBody()->getContents());
         return redirect('/project/mfajar');

         
    }
}
